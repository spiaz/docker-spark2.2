FROM ubuntu:16.04

LABEL maintainer="Stefano Piazza"

# Set working directory in /
WORKDIR /app

# Install pip
RUN apt-get update && \
    apt-get -y upgrade && \
    apt-get install -y curl apt-utils zip && \
    apt-get install -y python3-pip && \
    pip3 --no-cache-dir install --upgrade pip && \
    ln -s /usr/bin/python3 /usr/bin/python

# Install & configure Java
RUN apt-get update && apt-get install software-properties-common -y
RUN add-apt-repository ppa:webupd8team/java
RUN echo debconf shared/accepted-oracle-license-v1-1 select true | debconf-set-selections
RUN echo debconf shared/accepted-oracle-license-v1-1 seen true | debconf-set-selections
RUN apt-get update
RUN apt-get install oracle-java8-installer -y
ENV JAVA_HOME /usr/lib/jvm/java-8-oracle
ENV JRE_HOME /usr/lib/jvm/java-8-oracle/jre
ENV PATH "${PATH}:${JAVA_HOME}"
ENV PATH "${PATH}:${JRE_HOME}"

# Install & configure Scala
ENV SCALA_VERSION 2.11.8
RUN curl -fsL https://downloads.lightbend.com/scala/$SCALA_VERSION/scala-$SCALA_VERSION.tgz | tar xfz - -C /opt/
ENV SCALA_HOME /opt/scala-$SCALA_VERSION
ENV PATH "${PATH}:${SCALA_HOME}/bin"

# Install & configure Spark
ENV SPARK_VERSION 2.2.0
RUN curl -fsL https://archive.apache.org/dist/spark/spark-$SPARK_VERSION/spark-$SPARK_VERSION-bin-hadoop2.7.tgz | tar xfz - -C /opt/
ENV SPARK_HOME /opt/spark-$SPARK_VERSION-bin-hadoop2.7
ENV PATH "${PATH}:${SPARK_HOME}/bin"

# Add and install requirements
ADD requirements.txt /app/requirements.txt
RUN pip3 --no-cache-dir install -r requirements.txt

# Install jupyter-scala
RUN wget https://github.com/jupyter-scala/jupyter-scala/archive/master.zip &&\
    unzip master.zip &&\
    rm -f master.zip &&\
    jupyter-scala-master/jupyter-scala

EXPOSE 8888

VOLUME /app

CMD ["jupyter", "notebook", "--ip='*'", "--port=8888", "--no-browser", \
    "--allow-root"]